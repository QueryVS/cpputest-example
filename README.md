[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

# CppUTest

## Sources
- [Install and Starting Code(Github page)](http://cpputest.github.io/)
- [Coding Example(Github Page)](https://cpputest.github.io/manual.html)
- [Starting](https://www.sparkpost.com/blog/getting-started-cpputest/)

## Installing

1. Compile and install
1. Install package manager 

### 1. Compile and install 

```
user $ git clone git://github.com/cpputest/cpputest.git

user $ cd cpputest
user:cpputest/ $ cd build
user:cpputest/build $ rm -rf *
user:cpputest/build $ cmake ..
user:cpputest/build $ make 
root:cpputest/build $ make install
```
### Install package manager

#### Gentoo

> emerge --ask dev-util/cpputest

#### Debian

> apt-get install cpputest

## First Project 

### CppUTest Project main.c

