cmake_minimum_required(VERSION 3.9)

project(main C CXX)

set(ProjectFolder "project")
set(UnitTestFolder "unittest")

set(APP_NAME project/bin/project)
set(APP_LIB_NAME project/lib/libProject.a)


add_subdirectory(${ProjectFolder})
add_subdirectory(${UnitTestFolder})

message("FINISH MAIN PROJECT --> CMakeProject.txt")

