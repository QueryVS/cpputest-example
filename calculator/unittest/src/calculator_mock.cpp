// #include "CppUTest/TestHarness.h"

#include "CppUTestExt/MockSupport.h"

extern "C"{  // for added C code
	#include <calc.h> // my project header files for testing
}


TEST_GROUP(calculator){
	void setup(){
    	}

	void teardown(){
	}
};

TEST(calculator, simpleTest)
{
    // arrange
    mock().expectOneCall("add")
          .withParameter("number", 25,2)
          .andReturnValue(50);

    // act
    int add_s = subt(30, 5);

    // assert
    mock().checkExpectations();
    CHECK_EQUAL(50, add_s);
}


TEST(calculator, BlackBoxTest)
{
    // arrange
    mock().expectOneCall("add")
          .withParameter("number", 100,5)
          .andReturnValue(105); // intentionally set the result as 100.0 instead of 10.0.

    // act
    int subt_p = subt(8,6);

    // assert
    mock().checkExpectations();
    CHECK_EQUAL(2, subt_p);
}

